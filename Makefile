SHELL:=/bin/bash
.DEFAULT_GOAL := help

#
# main variables
#

PROJECT_NAME=prometheus
VERSION ?= $(shell git describe --always)
PROMETHEUS_VERSION ?= 2.45.0
ARCH=amd64
PROM_RPM_NAME = cactus-prometheus-${PROMETHEUS_VERSION}-${RELEASE}.${ARCH}.rpm
PROM_CONF_RPM_NAME = cactus-prometheus-config-${VERSION}-${RELEASE}.${ARCH}.rpm

#
# main targets
#

.PHONY: rpm 
rpm: ${PROM_RPM_NAME} ${PROM_CONF_RPM_NAME} ## Builds RPMs

.PHONY: conf
conf: ${PROM_CONF_RPM_NAME} ## Builds conf-only RPMs

.PHONY: clean 
clean: ## Cleans up project
	rm -rf rpms prom_rpmroot conf_rpmroot prometheus-${PROMETHEUS_VERSION}.linux-${ARCH}
#
# dependencies
#

PROMETHEUS_STR = prometheus-${PROMETHEUS_VERSION}.linux-${ARCH}
${PROMETHEUS_STR}:
	curl -LO https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/${PROMETHEUS_STR}.tar.gz
	tar xf ${PROMETHEUS_STR}.tar.gz
	rm -rf ${PROMETHEUS_STR}.tar.gz
	touch  ${PROMETHEUS_STR}

.PHONY: setup
setup: ${PROMETHEUS_STR}

${PROM_RPM_NAME}: ${PROMETHEUS_STR} *
	
	rm -rf prom_rpmroot
	mkdir -p rpms

	mkdir -p prom_rpmroot/opt/cactus/bin/prometheus/ prom_rpmroot/opt/cactus/usr/share/prometheus/
	cp ${PROMETHEUS_STR}/prometheus prom_rpmroot/opt/cactus/bin/prometheus/prometheus
	cp -r ${PROMETHEUS_STR}/console_libraries ${PROMETHEUS_STR}/consoles prom_rpmroot/opt/cactus/usr/share/prometheus/

	cd prom_rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME} \
	-v ${PROMETHEUS_VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--description "The Prometheus monitoring system and time series database." \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/prometheus" \
	--provides cactus-${PROJECT_NAME} \
	--provides prometheus \
	--provides prometheus2 \
	--replaces prometheus2 \
	--depends shadow-utils \
	--depends cactus-prometheus-config \
	.=/ && mv *.rpm ../rpms/

${PROM_CONF_RPM_NAME}: ${PROMETHEUS_STR} *
	
	rm -rf conf_rpmroot
	mkdir -p rpms
	
	mkdir -p conf_rpmroot/usr/lib/systemd/system conf_rpmroot/opt/cactus/etc/default/ conf_rpmroot/opt/cactus/etc/prometheus/ 
	cp cactus-prometheus.yaml conf_rpmroot/opt/cactus/etc/prometheus/
	cp -r rules conf_rpmroot/opt/cactus/etc/prometheus/
	cp default/cactus-prometheus.default conf_rpmroot/opt/cactus/etc/default/cactus-prometheus
	cp systemd/cactus-prometheus.service conf_rpmroot/usr/lib/systemd/system
	
	cd conf_rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME}-config \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--description "Configuration of the Prometheus monitoring system at P5." \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/prometheus" \
	--provides cactus-${PROJECT_NAME}-config \
	--depends shadow-utils \
	.=/ && mv *.rpm ../rpms/

.PHONY: start.docker
start.docker: build.docker ## Starts a docker container running prometheus, port 9090 is exposed
	docker run -p 9090:9090 -it prometheus

.PHONY: build.docker
build.docker: ## Builds the project in a docker container called prometheus
	docker build -t $(PROJECT_NAME) -f Dockerfile .

.PHONY: spellcheck.docker
spellcheck.docker: build.docker ## Spell checks conf files in docker
	docker run --entrypoint promtool -it prometheus check config /etc/prometheus/prometheus.yml

.PHONY: test.docker
test.docker: spellcheck.docker ## Test rules in docker
	docker run --entrypoint promtool -it prometheus test rules /etc/prometheus/tests/test_rate_alerting.yml

TESTFILES:=$(wildcard tests/*.yml tests/*.yaml)

.PHONY: test $(TESTFILES)
test: setup spellcheck $(TESTFILES) ## Test rules

$(TESTFILES): setup
	${PROMETHEUS_STR}/promtool test rules $@; \


.PHONY: spellcheck
spellcheck: setup ## Spellcheck config files
	${PROMETHEUS_STR}/promtool check config cactus-prometheus.yaml

.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
