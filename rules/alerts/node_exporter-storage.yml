groups:
- name: System storage rules
  rules:
# warns when used space on / > 70%
  - alert: NodeLowRootDisk
    expr: >-
      ((node_filesystem_size_bytes{mountpoint="/"} - node_filesystem_avail_bytes{mountpoint="/"} ) / node_filesystem_size_bytes{mountpoint="/"} * 100) > 70
    for: 2m
    labels:
      severity: "{{ if gt $value 90 }}error{{ else }}warning{{ end }}"
    annotations:
      title: Low root disk space
      url: "http://l1ts-grafana.cms/d/rYdddlPWk/prometheus-node-exporter-full?orgId=1&refresh=1m&var-node={{ $labels.instance }}"
      description: >-
        Root disk usage on {{$labels.instance}} is {{ $value | printf "%.2f" }}%. 
        Contact [L1T Online Software expert](https://twiki.cern.ch/twiki/bin/viewauth/CMS/OnlineWBTriggerContacts).
  
# warns when used space on /l1ts-prometheus-storage on prometheus > 70
  - alert: PrometheusLowDataDisk
    expr: >-
      ((node_filesystem_size_bytes{instance="l1ts-prometheus.cms:9100", mountpoint="/l1ts-prometheus-storage"} - node_filesystem_avail_bytes{instance="l1ts-prometheus.cms:9100", mountpoint="/l1ts-prometheus-storage"} ) / node_filesystem_size_bytes{instance="l1ts-prometheus.cms:9100", mountpoint="/l1ts-prometheus-storage"} * 100) > 70
    for: 2m
    labels:
      severity: "{{ if gt $value 90 }}error{{ else }}warning{{ end }}"
    annotations:
      title: Low root disk space
      url: "http://l1ts-grafana.cms/d/rYdddlPWk/prometheus-node-exporter-full?orgId=1&refresh=1m&var-node={{ $labels.instance }}"
      description: >-
        Data mountpoint usage on {{$labels.instance}} is {{ $value | printf "%.2f" }}%. 
        Contact [L1T Online Software expert](https://twiki.cern.ch/twiki/bin/viewauth/CMS/OnlineWBTriggerContacts).

   # warns if space used by prometheus > 90% and is expected to fill in 48h
  - alert: HostDiskWillFillIn48Hours
    expr: (node_filesystem_avail_bytes{instance="l1ts-prometheus.cms:9100", mountpoint="/l1ts-prometheus-storage"} * 100) / node_filesystem_size_bytes{instance="l1ts-prometheus.cms:9100", mountpoint="/l1ts-prometheus-storage"} < 10 AND ON (instance, device, mountpoint) predict_linear(node_filesystem_avail_bytes{instance="l1ts-prometheus.cms:9100", mountpoint="/l1ts-prometheus-storage"}[12h], 48 * 3600) < 0 
    for: 2m
    labels:
      severity: error
    annotations:
      title: Host disk will fill in 24 hours (instance {{ $labels.instance }})
      url: "http://l1ts-grafana.cms/d/rYdddlPWk/prometheus-node-exporter-full?orgId=1&refresh=1m&var-node={{ $labels.instance }}"
      description: >-
        Monitoring DB is predicted to run out of space within the next 48 hours at current write rate.
        Contact [L1T Online Software expert](https://twiki.cern.ch/twiki/bin/viewauth/CMS/OnlineWBTriggerContacts).

 # warns when swap usage > 70%
  - alert: NodeSwapUsage
    expr: >-
      (((node_memory_SwapTotal_bytes-node_memory_SwapFree_bytes)/node_memory_SwapTotal_bytes)*100) > 70
    for: 2m
    labels:
      severity: "{{ if gt $value 90 }}error{{ else }}warning{{ end }}"
    annotations:
      title: High swap usage detected
      url: "http://l1ts-grafana.cms/d/rYdddlPWk/prometheus-node-exporter-full?orgId=1&refresh=1m&var-node={{ $labels.instance }}"
      description: >-
        Swap usage on {{$labels.instance}} is {{ $value | printf "%.2f" }}%.
        Contact [L1T Online Software expert](https://twiki.cern.ch/twiki/bin/viewauth/CMS/OnlineWBTriggerContacts).

