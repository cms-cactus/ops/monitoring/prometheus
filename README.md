# Prometheus

## Download Prometheus

Checkout the repo, and download and extract Prometheus with:

```bash
make setup
```

## Starting Prometheus

Run:

```
PROMETHEUS_FOLDER/prometheus --config.file CONFIGURATION.yaml
```

Sadly without being in P5, the default configuration in ```cactus-prometheus.yaml``` will not work.

## Developing new features

[Checkout the Wiki!](https://gitlab.cern.ch/cms-cactus/ops/monitoring/prometheus/-/wikis/home)
