FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7:tag-0.0.8
COPY ci_rpms /rpms
RUN yum install -y /rpms/*.rpm && yum clean all
EXPOSE 9090